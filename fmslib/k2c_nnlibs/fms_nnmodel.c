#include <stdio.h> 
#include <math.h> 
#include "k2c_include.h"
#include "emmodel.h"
#include "xraymodel.h"


float results_array[20] = {0};

float * predict(float input_array[324], int em)
{


    k2c_tensor input_tensor = {&input_array[0],1,324,{324,  1,  1,  1,  1}};
    k2c_tensor results_tensor = {&results_array[0],1,20,{20, 1, 1, 1, 1}};

    if(em==1){
        emmodel_initialize();
        emmodel(&input_tensor, &results_tensor);
    } else {
        xraymodel_initialize();
        xraymodel(&input_tensor, &results_tensor);
    }

    //for(size_t i=0; i<results_tensor.numel; i++)
    //    printf("%f\n", results_tensor.array[i]);

    // keras2c model misses last logsoftmax layer (replaced with identity)
    float sum_exp=0;
    for(size_t i=0; i<results_tensor.numel; i++){
        results_tensor.array[i]=exp(results_tensor.array[i]);
        sum_exp+=results_tensor.array[i];
    }
    for(size_t i=0; i<results_tensor.numel; i++){
        results_tensor.array[i]/=sum_exp;
    }


    return results_tensor.array;

}


int main(void){}

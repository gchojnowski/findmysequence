#! /usr/bin/env libtbx.python

__author__ = "Grzegorz Chojnowski"
__date__ = "16 Feb 2021"

import os,sys,re

import torch
import torch.nn.functional as F
from torch import optim
from torch.utils.data import DataLoader
from torch.utils.data import TensorDataset




class naive(torch.nn.Module):

    def __init__(self):
        super().__init__()

        self.dense1 = torch.nn.Linear(in_features=324, out_features=324)
        self.dense2 = torch.nn.Linear(in_features=324, out_features=20)
        self.do = torch.nn.Dropout(p=0.5)

    def forward(self, state):

        #return F.log_softmax(self.dense2(state), dim=1)

        x = F.relu(self.dense1(state))
        x1 = self.do(x)
        outputs = self.dense2(x1)
        return F.log_softmax(outputs, dim=1)

class simple(torch.nn.Module):

    def __init__(self, nn=100):
        super().__init__()

        self.dense1 = torch.nn.Linear(in_features=324, out_features=nn)
        self.dense2 = torch.nn.Linear(in_features=nn, out_features=20)
        self.do = torch.nn.Dropout(p=0.5)

    def forward(self, state):

        x = F.relu(self.dense1(state))
        x1 = self.do(x)
        outputs = self.dense2(x1)
        return F.log_softmax(outputs, dim=1)

class simple_2l(torch.nn.Module):

    def __init__(self, nn=100):
        super().__init__()

        self.dense1 = torch.nn.Linear(in_features=324, out_features=nn)
        self.dense2 = torch.nn.Linear(in_features=nn, out_features=100)
        self.dense3 = torch.nn.Linear(in_features=100, out_features=20)
        self.do1 = torch.nn.Dropout(p=0.5)
        self.do2 = torch.nn.Dropout(p=0.5)

    def forward(self, state):

        x = F.relu(self.dense1(state))
        x1 = self.do1(x)
        x2 = F.relu(self.dense2(x1))
        x3 = self.do2(x2)
        outputs = self.dense3(x3)

        return F.log_softmax(outputs, dim=1)


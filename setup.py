
__author__ = "Grzegorz Chojnowski"
__date__ = "15 Dec 2021"

from setuptools import setup
import os,sys,glob,re
sys.path.insert(0, f'{os.path.dirname(__file__)}/findmysequence')
import subprocess
import distutils
import distutils.command.build
from distutils.command.install_lib import install_lib as _install_lib
from distutils.command.build_ext import build_ext
from distutils.core import setup, Extension


class build_ext(build_ext):

    def get_export_symbols(self, ext):
        return ext.export_symbols

    def get_ext_filename(self, ext_name):
        return ext_name + '.so'


class my_build(_install_lib):
    def run(self):
        subprocess.check_call('scons', cwd='./fmslib')
        _install_lib.run(self)


fms_nnmodel = Extension('fmslib/fms_nnmodel',
                            define_macros = [('MAJOR_VERSION', '1'),
                                     ('MINOR_VERSION', '0')],
                        include_dirs = ["fmslib/keras2c/include/", "fmslib/keras2c/", "fmslib/k2c_nnlibs/"],
                        extra_compile_args = ['-O3', '-fPIC', '-std=c99', '-Wno-strict-prototypes'],
                        sources = ["fmslib/k2c_nnlibs/fms_nnmodel.c", 
                                   "fmslib/k2c_nnlibs/emmodel.c", 
                                   "fmslib/k2c_nnlibs/xraymodel.c"]+glob.glob("fmslib/keras2c/include/*.c"))



def dependencies():
    with open("fmslib/pip-req.txt", "r") as f_in:
        deps = f_in.read().splitlines()
    return deps


def readme():
    with open("README.rst", "r") as f_in:
        return f_in.read()

def get_git_describe(abbrev=7):
    import findmysequence.version
    return findmysequence.version.__version__

    try:
        p = subprocess.Popen(['git', 'describe', '--abbrev=%d' % abbrev],
                  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.stderr.close()
        line = p.stdout.readlines()[0]
        version = line.strip().decode('ascii')
        # remove pep440-incompatible sha tag
        if len(version.split('-'))>1:
            version = ".".join(re.split(r'[-.]',version)[:-1])

        with open("findmysequence/version.py", "w") as ofile:
            ofile.write("__version__=\"%s\""%version)
        return version
    except:
        return None

_package_data={'fmslib':[os.path.join('data','*'),\
                         os.path.join('data','mon_lib','*','*'),\
                         os.path.join('data','geostd','*','*'),\
                         os.path.join('examples','*')] }

if sys.platform.startswith('win'):
    _package_data['fmslib'].extend(['fms_nnmodel_win64.dll', os.path.join('libexec','*')])
else:
    # makes nn modules are ready before installation starts - osx/linux only
    print("***** Compiling NN modules")
    ret=subprocess.check_call('scons', cwd='fmslib')
    _package_data['fmslib'].append('fms_nnmodel.so')

_ext_modules=[]
_cmdclass={}

setup(
    name="findmysequence",
    ext_modules=_ext_modules,
    version=get_git_describe(),
    packages=["findmysequence", "fmslib"],
    cmdclass=_cmdclass,
    package_data=_package_data,
    url="https://gitlab.com/gchojnowski/findmysequence",
    license="BSD",
    platforms=["Mac OS", "Unix"],
    author="Grzegorz Chojnowski",
    author_email="gchojnowski@embl-hamburg.de",
    description="Sequence indentification and assignment in cryo-EM and crystal structure protein models",
    long_description=readme(),
    zip_safe=False,
    entry_points={
          "console_scripts": [
            "findmysequence = findmysequence.__main__:main",
            ],
    }
)

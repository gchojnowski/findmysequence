# findMySequence: tool for finding and assigning protein model sequences in EM and MX

- [How to cite](#how-to-cite)
- [Installation](#installation)
<!-- 
   - [using anaconda package](#automated-installation-from-anaconda)--->
   - [Installing from source](#source-installation)
- [Test your installation](#test-your-installation)
- [How to use findMySequence](#how-to-use-findmysequence)
    - [program input](#program-input)
    - [sequence search](#sequene-search)
    - [sequence assignment](#sequence-assignment)

# How to cite

findMySequence: a neural-network-based approach for identification of unknown proteins in X-ray crystallography and cryo-EM\
Grzegorz Chojnowski, Adam J. Simpkin, Diego A. Leonardo, Wolfram Seifert-Davila, Dan E. Vivas-Ruiz, Ronan M. Keegan, Daniel J. Rigden 
[IUCrJ 9.1 (2022)](https://journals.iucr.org/m/issues/2022/01/00/pw5018/)

# Installation

The program was tested mostly on Linux and MacOS. It will also work on MS Windows inlcuding the Windows Subsystem for Linux (WLS).

The program has (soft) dependencies on various programs distributed with [CCP4 suite](https://www.ccp4.ac.uk/). It can use HMMER suite and PDB100 sequence database distributed with CCP4. If you don't have CCP4 installed, don't worry; the program will run normally.

For handling software dependencies *findMySequence* requires **conda** or **miniconda** environments. Detailed installation instructions for each of them can be found [here](https://docs.conda.io/en/latest/index.html) and [here](https://docs.conda.io/en/latest/miniconda.html). After installing conda (if needed) create and activate an enironment:

```
conda create -n findmysequence
conda activate findmysequence
```

alternatively, you can use miniconda. It doesn't require any changes to you system and it's fully contained in a local directory. To install it download an installer from [here](https://docs.conda.io/en/latest/miniconda.html) and run the following (linux example):

```
bash Miniconda3-latest-Linux-x86_64.sh -b -p miniconda_findmysequence
. miniconda_findmysequence/bin/activate
```

Now, you can proceed with the installation instructions below

### Source installation 

With this version of *findMySequence* you can skip installation of a pretty heavy pytorch library in the list below. A lightweight, C-based NN module will be automatically compiled during installation. If this fails, simply install pytorch using the command below and rerun installation (see below). 

First, install dependencis using conda

```
conda install -c conda-forge -c bioconda cctbx-base scipy scons hmmer boost
```

If you are installing *findMySequence* on MS Windows (not WLS!) remove hmmer from the list above


Next, clone repository and run installation script.

```
git clone https://gitlab.com/gchojnowski/findmysequence.git
cd findmysequence
python setup.py install
```
##### Test your installation

```
findmysequence --test
```

##### Troubleshooting

If the standard installation procedure above fails (can happen on older Linux installations or WLS), try to install latest c/c++ compilers with conda and rerurn instalation script

```
conda install -c conda-forge gcc gxx
python setup.py install
```

# How to use findMySequence

Given a protein model and a map the program can either find the most plausible sequence in a database ([sequene search mode](#sequence-search)) or assign a fragment of a model to a target sequende and build side-chains ([sequence assignment mode](#sequence-assignment)). 


### program input

The program accepts input models in PDB and mmCIF formats. For EM models provide an MRC/CCP4 map file. For crystal structure models the map must be provided as an MTZ file with structure factor columns defined using *--labin* keyword (--labin=amplitudes,phases e.g. *--labin=FWT,PHWT*). The following examples use a retinoic acid bining protein crystal strucutre ([1cbs](https://www.rcsb.org/structure/1CBS)) and an obsolete cryo-EM model of haemoglobin at 3.2A resolution ([5me2](https://www.rcsb.org/structure/5ME2)). Both structures and maps are available in the project ```examples``` directory.

### sequence identification

Given a main-chain only protein model (side-chains are ignored) and cryo-EM reconstruction or crystallographic map the program finds the most plausible sequence in a database. The database can be any text file with set of sequences in fasta format. If CCP4 is available *findMySequence* will use by default a set of all PDB sequences distributed with MrBump.

```
findmysequence --mtzin examples/1cbs_final.mtz --labin FWT,PHWT --modelin examples/1cbs_final.pdb 
```

to query a local sequence database with a cryo-EM model:

```
findmysequence --mapin examples/emd_3488.map --modelin examples/5me2.pdb --db examples/example_db.fa
```
 
this should produce the following output

```
>5NI1_1|Chains|E-value=1.10e-38
VLSPADKTNVKAAWGKVGAHAGEYGAEALERMFLSFPTTKTYFPHFDLSHGSAQVKGHGKKVADALTNAVAHVDDMPNALSALSDLHAHKLRVDPVNFKLLSHCLLVTLAAHLPAEFTPAVHASLDKFLASVSTVLTSKYR
>5NI1_2|Chains|E-value=8.10e-28
VHLTPEEKSAVTALWGKVNVDEVGGEALGRLLVVYPWTQRFFESFGDLSTPDAVMGNPKVKAHGKKVLGAFSDGLAHLDNLKGTFATLSELHCDKLHVDPENFRLLGNVLVCVLAHHFGKEFTPPVQAAYQKVVAGVANALAHKYH
```

Note that the input haemoglobin model contains both alpha and beta chains and the query returned both. In such cases you will need to indentify chains based on sequence assignment p-values (see below).

By default the program makes queries based on all the protein residues in the input model, but a user defined fragment (e.g. most reliable) can be also specified with *--select* keyword.

```
findmysequence --mtzin examples/1cbs_final.mtz --labin FWT,PHWT --modelin examples/1cbs_final.pdb --db examples/example_db.fa --select "chain A and (resi :50 or resi 60:)"
```

By default the program uses HMMER suite to match residue-type probabilities derived from a map and model to target sequences. It can also use a slower sliding-window algorithm (activated with *--slide* keyword). It assumes that input model is **continous and doesn't contain tracing errors** and thus may give more accurate results in difficult cases. For example, when query is a short alpha-helical model. **Important:** in this mode, only the longest continous protein fragment from the input strucuture is used for queries (unless a specific fragment is selected with *--select* keyword) 

```
findmysequence --mtzin examples/1cbs_final.mtz --labin FWT,PHWT --modelin examples/1cbs_final.pdb --db examples/example_db.fa --slide
```

The output p-value corresponds to a probability that a sequence assignment is purely random. As a rule of thumb, sequence matches with p-value below 1e-1 are correct in most cases.

### sequence assignment

To assign input model to a sequence and build side-chains you must provide a input map and a model, target sequence (*--seqin*) and output model filename (*--modelout*)  e.g.

```
findmysequence --mtzin examples/1cbs_final.mtz --labin FWT,PHWT --modelin examples/1cbs_final.pdb --seqin examples/1cbs.fa --select "chain A and (resi :50 or resi 60:)" --modelout docked_fragment.pdb
```

this should produce the following output

```
 ==> Aligning chain fragments to the input sequence
         1  A     1:50    @     1:50    p-value=1.479089e-17
            PNFSGNWKIIRSENFEELLKVLGVNVMLRKIAVAAASKPAVEIKQEGDTF
            PNFSGNWKIIRSENFEELLKVLGVNVMLRKIAVAAASKPAVEIKQEGDTF
            62555563352451124363565535362355555553554354145455

         2  A    60:137   @    60:137   p-value=1.006555e-21
            TTEINFKVGEEFEEQTVDGRPCKSLVKWESENKMVCEQKLLKGEGPKTSWTRELTNDGELILTMTADDVVCTRVYVRE
            TTEINFKVGEEFEEQTVDGRPCKSLVKWESENKMVCEQKLLKGEGPKTSWTRELTNDGELILTMTADDVVCTRVYVRE
            552553345322212552536245353634252151434673535635415215542523535265545525244523

 ==> Wrote modified model to:
      docked_fragment.pdb
```
The output p-value corresponds to a probability that the sequence assignment is purely random. In general, sequence assignments with p-value below 1e-1 are correct in vast majority of cases. In case there are many alternative sequences for a chains, always choose the one with the lower p-value (see checkMySequence publication for details [Acta Cryst. (2022). D78](https://scripts.iucr.org/cgi-bin/paper?S2059798322005009)).


(C) 2021 Grzegorz Chojnowski  

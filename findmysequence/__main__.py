#! /usr/bin/env python

__author__ = "Grzegorz Chojnowski"
__date__ = "08 Apr 2021"


import os,sys,re
import json
from shutil import which
import traceback

from optparse import OptionParser, OptionGroup, SUPPRESS_HELP

ROOT = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(2, os.path.join(ROOT, '..', 'fmslib'))
sys.path.insert(2, os.path.join(ROOT, '..'))

import numpy as np

import fmslib
from fmslib import xyz_utils
from fmslib import sequence_utils


try:
    import findmysequence.version
    version = findmysequence.version.__version__
except:
    version="dev"

fms_header=f"""
# findMySequence(v{version}): tool for finding and assigning protein model sequences in EM and MX

HMMER is {'' if sequence_utils.HMMER_AVAILABLE else "NOT "}available{" (from CCP4)" if sequence_utils.HMMER_FROM_CCP4 else ""}
"""


def parse_args():
    """setup program options parsing"""
    parser = OptionParser()

    choices_ebiapi = ('uniprot', 'swissprot')

    required_opts = OptionGroup(parser, "Required parameters (model and a map or an mtz file with labels)")
    parser.add_option_group(required_opts)

    required_opts.add_option("--modelin", action="store", \
                            dest="modelin", type="string", metavar="FILENAME", \
                  help="PDB/mmCIF model", default=None)

    required_opts.add_option("--mapin", action="store", dest="mapin", type="string", metavar="FILENAME", \
                  help="Map in CCP4 format", default=None)

    required_opts.add_option("--mtzin", action="store", dest="mtzin", type="string", metavar="FILENAME", \
                  help="input sfs in MTZ format", default=None)

    required_opts.add_option("--labin", action="store", dest="labin", type="string", metavar="F,PHI", \
                  help="MTZ file column names", default=None)


    extra_opts = OptionGroup(parser, "Extra options")
    parser.add_option_group(extra_opts)

    extra_opts.add_option("--select", action="store", dest="selstr", type="string", metavar="STRING", \
                  help="fragments selection string"\
                       " eg. \"resi 10:50 and chain A\" [default: %default]", default="all")

    extra_opts.add_option("--tophits", dest="tophits_sto", type="int", default=3, metavar="VALUE", \
                  help="Number of top sequence hits to print [default: %default]")

    extra_opts.add_option("--jsonout", action="store", dest="jsonout", type="string", metavar="FILENAME", \
                  help="output filename with the sequence identification or assignment results in json format (machine readable)", default=None)

    extra_opts.add_option("--ebiapi", type="choice", choices=choices_ebiapi, dest="ebiapi", default=None, \
            help="""query whole UniProt using HMMER API on EMBL-EBI servers. they (usually) work great and """\
                 """incredibly fast. If you need a whole UniProt scan it's worth to use this option. Alternatively, """\
                 """you van export HMM model with --hmmout options and run the search on your own.""")

    extra_opts.add_option("--hmmout", action="store", dest="hmmout", type="string", metavar="FILENAME", \
            help="output file with the HMM, use with: hmmsearch --noali --tblout hmmsearch.log FILENAME DATABASE", default=None)

    default_ccp4_db=None
    if sequence_utils.CCP4_ENV:
        for fn in ['pdb100.txt', 'pdbALL.txt']:
            _default_ccp4_db = os.path.join(sequence_utils.CCP4_ENV, 'share', 'mrbump', 'data', fn)
            if os.path.exists(_default_ccp4_db):
                default_ccp4_db = _default_ccp4_db
                break

    # DB-serach related options
    search_opts = OptionGroup(parser, "Database search options (default mode)")
    parser.add_option_group(search_opts)

    search_opts.add_option("--slide", action="store_true", dest="slide", default=True if sequence_utils.HMMER_AVAILABLE is None else False, \
            help="Use naive sliding-window algorithm for db search (much slower than default hmmer) [default: %default]")

    search_opts.add_option("--db", action="store", \
                            dest="db", type="string", metavar="FILENAME", \
                            help="Sequence database (may be gzipped) [default: %default]", default=default_ccp4_db)


    # sequende assignment specific options
    dock_opts = OptionGroup(parser, "Sequence assignement options (both arguments below are required)")
    parser.add_option_group(dock_opts)


    dock_opts.add_option("--seqin", action="store", dest="seqin_fname", type="string", metavar="FILENAME", \
                  help="targt sequence for sequene assignment", default=None)

    dock_opts.add_option("--modelout", action="store", dest="modelout", type="string", metavar="FILENAME", \
                  help="output filename for the sequence assignment", default=None)


    # developer opts
    parser.add_option("--debug", action="store_true", dest="debug", default=False, \
                  help=SUPPRESS_HELP)

    parser.add_option("--refseq", action="store", dest="refseq_fname", type="string", metavar="FILENAME", \
                  help=SUPPRESS_HELP if 1 else """reference sequence in fasta format (for testing, will print 
                                                    sequence identity to all hits)""", default=None)

    parser.add_option("--test", action="store_true", dest="test", default=False, \
                  help="a simple test")

    required_opts.add_option("--badd", action="store", dest="badd", type="float", metavar="FLOAT", \
                  help=SUPPRESS_HELP, default=0)


    (options, _args)  = parser.parse_args()
    return (parser, options)


# -----------------------------------------------------------------------------


def guess(mapin         =   None,
          mtzin         =   None,
          labin         =   None,
          modelin       =   None,
          db            =   None,
          selstr        =   None,
          refseq_fname  =   None,
          modelout      =   None,
          slide         =   False,
          tophits_sto   =   3,
          badd          =   0,
          jsonout       =   None,
          hmmout        =   None,
          ebiapi        =   None,
          debug         =   False):

    m2so = sequence_utils.model2sequence( mapin=mapin, mtzin=mtzin, labin=labin, badd=badd)
    ph, symm = m2so._xyz_utils.read_ph( modelin )
    msa_string = m2so.model2msa( ph, selstr=selstr, verbose=False )

    if ebiapi:
        print(f" ==> Querying {ebiapi.upper()} database using HMMER API (remote service at www.ebi.ac.uk)")
    else:
        print(" ==> Querying database using %s: %s" % ('sliding window' if slide else 'hmmer', db))
    if slide:
        res = m2so.query_slide(db_fname=db, refseq_fname=refseq_fname, verbose=debug, tophits_sto=tophits_sto)
    else:
        res = m2so.query_msa( msa_string, db_fname=db, refseq_fname=refseq_fname, verbose=debug, tophits_sto=tophits_sto, hmmout=hmmout, ebiapi=ebiapi)

    results_dict = {}
    if res is None:
        print(" ==> No matches found...")
    else:
        print(" ==> Best matches\n")
        for v in res:
            print( v[-1] )

        # from _r[2] skips 1st line (fasta header)
        for idx, _r in enumerate(res):
            results_dict[idx+1]=dict([('sequence_id',_r[0]),
                                      ('evalue',_r[1]),
                                      ('sequence',"".join(_r[2].splitlines()[1:]))])

    if jsonout:
        with open(jsonout, 'w') as ofile:
            ofile.write(json.dumps(results_dict, sort_keys=True, indent=4))
        print(f" ==> Output wrote to {jsonout}")

# -----------------------------------------------------------------------------

def assign_sequence(mapin         =   None,
                    mtzin         =   None,
                    labin         =   None,
                    modelin       =   None,
                    db            =   None,
                    selstr        =   None,
                    seqin_fname   =   None,
                    modelout      =   None,
                    slide         =   False,
                    jsonout       =   None,
                    debug         =   False):

    from iotbx.bioinformatics import any_sequence_format

    _seq, _err = any_sequence_format(seqin_fname)

    assert (_seq is not None), "failed to parse input sequence"
    assert (len(_seq)==1), "only one sequence should be given on input"

    m2so = sequence_utils.model2sequence( mapin=mapin, mtzin=mtzin, labin=labin )
    ph, symm = m2so._xyz_utils.read_ph( modelin )
    msa_string = m2so.model2msa( ph, selstr=selstr, verbose=False )

    print( " ==> Aligning chain fragments to the input sequence" )
    with open(seqin_fname, 'r') as ifile:
        results_dict = m2so.align_frags(target_sequence=ifile.read(), modelout=modelout, verbose=debug)

    if jsonout:
        with open(jsonout, 'w') as ofile:
            ofile.write(json.dumps(results_dict, sort_keys=True, indent=4))

        print(f" ==> Output wrote to {jsonout}")

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def main():


    print(fms_header)

    (parser, options) = parse_args()


    if not options.debug:
        sys.tracebacklimit=0

    print( " ==> Command line: %s" % (" ".join(sys.argv)) )

    if options.test:

        print("\n")
        print( " *** Testing EM search\n")
        guess( mapin    = os.path.join(xyz_utils.ROOT, 'examples', 'emd_3488.map'),
               modelin  = os.path.join(xyz_utils.ROOT, 'examples', '5me2.pdb'),
               selstr   = "chain A and resi 20:40",
               slide    = True,
               debug    = options.debug,
               db       = os.path.join(xyz_utils.ROOT, 'examples', 'example_db.fa'))

        print("\n")
        print( " *** Testing XRAY search")
        guess( mtzin    = os.path.join(xyz_utils.ROOT, 'examples', '1cbs_final.mtz'),
               labin    = "FWT,PHWT",
               modelin  = os.path.join(xyz_utils.ROOT, 'examples', '1cbs_final.pdb'),
               selstr   = "chain A and resi 20:40",
               slide    = True if sequence_utils.HMMER_AVAILABLE is None else False,
               debug    = options.debug,
               db       = os.path.join(xyz_utils.ROOT, 'examples', 'example_db.fa'))


        print("\n")
        print( " *** Testing sequence assignment in EM")
        assign_sequence( mapin        =   os.path.join(xyz_utils.ROOT, 'examples', 'emd_3488.map'),
                         modelin      =   os.path.join(xyz_utils.ROOT, 'examples', '5me2.pdb'),
                         selstr       =   "chain A and resi :20",
                         seqin_fname  =   os.path.join(xyz_utils.ROOT, 'examples', '5me2_A.fa'),
                         modelout     =   None)

        exit(0)

    valid_params_no = len(sys.argv[1:]) - len(parser.largs)

    # no recognized params on input, print help message and exit...
    if not valid_params_no:
        parser.print_help()
        print
        exit(0)

    if options.modelin is None:

        print("ERROR: Input model missing")
        print()
        exit(1)

    if options.mapin is None and \
        (options.mtzin is None and options.labin is None):

        print("ERROR: Input map or mtz file with labels missing")
        print()
        exit(1)


    if [options.seqin_fname,options.modelout].count(None)==1:
        print("ERROR: Both seqin and modelout are required for the sequence asignment")
        print()
        exit(1)

    if options.seqin_fname and options.modelout:
        assign_sequence( mapin        =   options.mapin,
                         mtzin        =   options.mtzin,
                         labin        =   options.labin,
                         modelin      =   options.modelin,
                         db           =   options.db,
                         selstr       =   options.selstr,
                         seqin_fname  =   options.seqin_fname,
                         modelout     =   options.modelout,
                         slide        =   options.slide,
                         jsonout      =   options.jsonout,
                         debug        =   options.debug)
        exit(0)


    if options.ebiapi is None and (options.db is None or not os.path.exists(options.db)):
        print("ERROR: Sequence database not available")
        print()
        exit(1)


    guess( mapin        =   options.mapin,
           mtzin        =   options.mtzin,
           labin        =   options.labin,
           modelin      =   options.modelin,
           db           =   options.db,
           selstr       =   options.selstr,
           refseq_fname =   options.refseq_fname,
           modelout     =   options.modelout,
           slide        =   options.slide,
           tophits_sto  =   options.tophits_sto,
           badd         =   options.badd,
           jsonout      =   options.jsonout,
           hmmout       =   options.hmmout,
           ebiapi       =   options.ebiapi,
           debug        =   options.debug)



if __name__=="__main__":
    main()
